# pGoL

a progressive implementation Game of Life with SDL in C++  

## Acknowledgment
This project utilizes the great [Simple DirectMedia Layer (SDL) library](https://www.libsdl.org/index.php) under the the [zlib license](https://www.zlib.net/zlib_license.html), which provides simple yet powerful, cross-platform interface to GUI.  
This project is packed with a subset of [LXGW WenKai](https://github.com/lxgw/LxgwWenKai), a wonderful Chinese font derived from Fontworks' Klee One which is released under [SIL OFL](https://scripts.sil.org/OFL).