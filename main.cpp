// pGoL: a progressive implementation Game of Life with SDL in C++
// Copyright (C) 2023 jonabom881
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program.  If not,
// see <http://www.gnu.org/licenses/>.
#include <config.hpp>

#include <SDL2/SDL.h>
#include <SDL_ttf.h>
#include <array>
#include <barrier>
#include <chrono>
#include <deque>
#include <format>
#include <iostream>
#include <random>
#include <span>
#include <thread>
#include <tuple>
#include <vector>
constexpr int InitialWindowWidth = 880;
constexpr int InitialWindowHeight = 660;
constexpr size_t BoardWidth = 40;
constexpr size_t BoardHeight = 40;
// space reserved for sidebar
constexpr float SidebarWidth = 120;
constexpr int FontPoints = 24;
class Board {
private:
  // number of cells on each row
  size_t width;
  // number of cells in each column
  size_t height;
  // keep two set of grids: we should use one of them as the current state and the other as the next state
  //  It is obvious that for each cell, a single bit is all you need to represent its state (alive/dead), but
  //  we used uint8_t instead of bool. Why?
  std::array<std::vector<uint8_t>, 2> grids;
  // or current state/grid
  std::vector<uint8_t> *front_grids;
  // or next state/grid
  //  the name front/back is borrowed from front/back buffer in computer graphics
  std::vector<uint8_t> *back_grids;
  // a barrier to sync worker threads and main thread
  //  To accelerate the procedure updating state of cells, we employed a multi-threaded manner that each
  //  thread will take care of cells in some continuos rows. To avoid the cost introduced by spawning and
  //  destroying threads dynamically for each iteration, we keep these worker threads running once a instance
  //  of Board is constructed and make them detached/daemonized. However, the state may only be updated at
  //  certain point determined by the main thread, while the main thread also need to be notified when next
  //  state of all cells has been calculated and stored properly. This is what exactly such barrier is used
  //  for. See update_worker for more detail.
  std::barrier<> worker_execute_barrier;

  // tiny helping function that retrieves the state of cell from the grid specified
  //  the main purpose of this function is to handle cases that the coordinate exceeds the boundary of board
  static inline uint8_t get_cell(
      const decltype(Board::front_grids) grids, const size_t width, const size_t height, size_t i, size_t j
  ) {
    if (i >= height || j >= width) {
      // such coordinate/index is out of range, simply return 0(dead) since all we care about is how many
      //  neighbors are alive
      // A note on the conditional expression: all values in it are typed (const) size_t, which is an instance
      // of the unsigned integer type class. Since the coordinate/index starts with 0, all coordinates that
      // exceeds the boundary shall be greater than or equal to the number of cells on that direction, even if
      // we are doing some calculation like get_cell(..., 0ul-1, 0ul-1). The result of assigning negative
      // integer to unsigned integer type is specified by the C++ standard and is totally predictable.
      return 0;
    }
    return (*grids)[i * width + j];
  }

  // procedure for each working thread
  static void update_worker(
      const int index, // index of this worker, which is used to calculate the range of rows to take care of
      // for the size of board, we use references since it may change after working threads are created
      const decltype(Board::width) &width,       // width of grid
      const decltype(Board::height) &height,     // height of grid
      decltype(Board::front_grids) &front_grids, // the front grid, or current state
      decltype(Board::back_grids) &back_grids,   // the back grid, or next state to be calculated
      std::barrier<> &worker_execute_barrier     // barrier for syncing
  ) {
    // keep threads running with a infinite loop
    while (true) {
      // sync at the barrier. When this call returns, all working threads are ready for calculation and the
      // main thread has issued clearance to such calculation
      worker_execute_barrier.arrive_and_wait();
      // calculate number of lanes this thread shall take case of in this iteration
      //  since the size of board may change, we need to recalculate this for each iteration
      const size_t lines =
          (height + std::thread::hardware_concurrency()) / std::thread::hardware_concurrency();
      // convert it to a range of rows, [low, high)
      const size_t low = index * lines;
      const size_t high = std::min(low + lines, height);
      // for each cell on each row that we take care of
      for (size_t i = low; i < high; i++) {
        for (size_t j = 0; j < width; j++) {
          uint8_t total = 0;
          for (int i_offset = -1; i_offset <= 1; i_offset++) {
            for (int j_offset = -1; j_offset <= 1; j_offset++) {
              if (i_offset == 0 && j_offset == 0) {
                // we do not count the cell itself
                continue;
              }
              // count the number of cells alive with the helping function
              total += Board::get_cell(front_grids, width, height, i + i_offset, j + j_offset);
            }
          }
          // simply apply the rule and write next state of this cell to the back grid
          if ((*front_grids)[i * width + j] == 0) {
            if (total == 3) {
              (*back_grids)[i * width + j] = 1;
            } else {
              (*back_grids)[i * width + j] = 0;
            }
          } else {
            if (total == 2 || total == 3) {
              (*back_grids)[i * width + j] = 1;
            } else {
              (*back_grids)[i * width + j] = 0;
            }
          }
        }
      }
      // done for this iteration, sync at the barrier again
      //  all threads, including the main thread shall sync here, and when this call returns, all working
      //  threads has done for this iteration
      worker_execute_barrier.arrive_and_wait();
    }
  }

public:
  Board(size_t width, size_t height)
      : width(width), height(height),
        // all threads that shall sync with this barrier: std::thread::hardware_concurrency() working threads
        // and the main thread
        worker_execute_barrier(std::thread::hardware_concurrency() + 1) {
    for (auto &grid : this->grids) {
      grid.resize(width * height); // make space for each grid
    }
    // set front/back grid
    this->front_grids = std::addressof(this->grids.at(0));
    this->back_grids = std::addressof(this->grids.at(1));
    // initialize front grid randomly
    std::uniform_int_distribution<uint8_t> random(0, 1);
    std::mt19937_64 generator{std::random_device()()};
    for (auto &item : *this->front_grids) {
      item = random(generator);
    }
    // spawn working threads
    for (size_t i = 0; i < std::thread::hardware_concurrency(); i++) {
      std::thread(
          Board::update_worker, i, std::ref(this->width), std::ref(this->height), std::ref(this->front_grids),
          std::ref(this->back_grids), std::ref(this->worker_execute_barrier)
      )
          .detach();
    }
  }
  // update state, calculate next state of front grid and save to back grid
  void update() {
    // trigger all working threads for calculation
    this->worker_execute_barrier.arrive_and_wait();
    // wait for the calculation
    this->worker_execute_barrier.arrive_and_wait();
    // swap front and back grid
    std::swap(this->back_grids, this->front_grids);
  }
  // interfaces to view private member
  std::tuple<
      decltype(Board::width), decltype(Board::height),
      const std::span<const std::remove_reference<decltype(front_grids->front())>::type>>
  get_grid() const {
    return std::make_tuple(
        this->width, this->height,
        std::span<const std::remove_reference<decltype(front_grids->front())>::type>(
            (*this->front_grids).cbegin(), (*this->front_grids).cend()
        )
    );
  }
};
// pure virtual class (interface) representing the capability of a frontend
//  up to now, we model frontend as singleton
class FrontEnd {
protected:
  // provide a default, trivial constructor
  FrontEnd() = default;

public:
  // provide a default, trivial destructor
  virtual ~FrontEnd() = default;
  // since the resource held by frontend shall be uniquely owned, make it not copyable or movable explicit
  FrontEnd(const FrontEnd &other) = delete;
  FrontEnd &operator=(const FrontEnd &other) = delete;
  FrontEnd(FrontEnd &&other) = delete;
  FrontEnd &operator=(FrontEnd &&other) = delete;
  virtual void draw(const Board &board) = 0;
  [[nodiscard]] virtual bool check_exit() const = 0;
};
// class representing a SDL frontend
//  we mark it as final to forbid further inherit
class SDLFrontEnd final : public FrontEnd {
private:
  // a holder for texture of text
  struct TextTexture {
    enum class Quality { Solid, Shaded, Blended };
    SDL_Texture *texture; // the texture
    int height;           // height of the texture
    int width;            // width of the text
    TextTexture(const char *text, TTF_Font *font, SDL_Color color, Quality quality, SDL_Renderer *renderer) {
      // we need to create a surface with SDL_ttf and then convert it to a texture
      SDL_Surface *surface = nullptr;
      switch (quality) {
      case Quality::Solid:
        surface = TTF_RenderUTF8_Solid(font, text, color);
        break;
      case Quality::Shaded:
        surface = TTF_RenderUTF8_Shaded(font, text, color, {255, 255, 255, SDL_ALPHA_TRANSPARENT});
        break;
      case Quality::Blended:
        surface = TTF_RenderUTF8_Blended(font, text, color);
        break;
      }
      // record the size
      this->height = surface->h;
      this->width = surface->w;
      this->texture = SDL_CreateTextureFromSurface(renderer, surface);
      SDL_FreeSurface(surface);
    }
    ~TextTexture() { SDL_DestroyTexture(this->texture); }
    // no copy, but you can move TextTexture around
    TextTexture(const TextTexture &other) = delete;
    TextTexture &operator=(const TextTexture &other) = delete;
    TextTexture(TextTexture &&other) noexcept
        : texture(other.texture), height(other.height), width(other.width) {
      other.texture = nullptr;
    }
    TextTexture &operator=(TextTexture &&other) noexcept {
      if (this != std::addressof(other)) {
        this->texture = other.texture;
        this->height = other.height;
        this->width = other.width;
        other.texture = nullptr;
      }
      return *this;
    }
  };
  SDL_Window *window;
  SDL_Renderer *renderer;
  TTF_Font *font;
  std::unique_ptr<TextTexture> fps;
  // some very aggressive and likely abnormal way to calculate FPS
  std::deque<decltype(std::chrono::system_clock::now())> time_of_frames;
  SDLFrontEnd() {
    // initialize SDL
    //  according to the documentation, SDL_INIT_VIDEO implies SDL_INIT_EVENTS
    SDL_Init(SDL_INIT_VIDEO);
    // create a window
    this->window = SDL_CreateWindow(
        "Game of Life",                                   // title of window
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, // do not specify the initial position of window
        InitialWindowWidth, InitialWindowHeight,          // initial size of window
        // this window shall be shown, backed by Vulkan, with HighDPI support and resizable
        SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_RESIZABLE
    );
    // handle errors
    if (this->window == nullptr) {
      std::cerr << std::format("Failed to create window: {}\n", SDL_GetError());
      exit(EXIT_FAILURE);
    }
    // create a renderer for the window we just created
    this->renderer = SDL_CreateRenderer(
        this->window,            // the window
        -1,                      // any rendering driver SDL sees suitable
        SDL_RENDERER_ACCELERATED // try to request for hardware accelerate
    );
    // handle errors
    if (this->renderer == nullptr) {
      std::cerr << std::format("Failed to create renderer: {}\n", SDL_GetError());
      exit(EXIT_FAILURE);
    }

    // init SDL_ttf
    if (TTF_Init() == -1) {
      std::cerr << std::format("Failed to initialize SDL_ttf: {}\n", TTF_GetError());
      exit(EXIT_FAILURE);
    }
    // open font
    this->font = TTF_OpenFont("font.ttf", FontPoints);
    if (this->font == nullptr) {
      std::cerr << std::format("Failed to open font: {}\n", TTF_GetError());
      exit(EXIT_FAILURE);
    }
    // create constant string textures
    this->fps = std::make_unique<TextTexture>(
        "帧每秒", this->font, SDL_Color{255, 255, 255, SDL_ALPHA_OPAQUE}, TextTexture::Quality::Blended,
        this->renderer
    );
  }

public:
  // get the singleton instance
  static SDLFrontEnd &getInstance() {
    static SDLFrontEnd sdl_frontend;
    return sdl_frontend;
  }
  ~SDLFrontEnd() override {
    // cleanup
    TTF_CloseFont(this->font);
    this->font = nullptr;
    SDL_DestroyRenderer(renderer);
    this->renderer = nullptr;
    SDL_DestroyWindow(window);
    this->window = nullptr;
    SDL_Quit();
  }
  // draw current state of board
  void draw(const Board &board) override {
    // get board information
    auto [width, height, grid] = board.get_grid();

    // clean the frame with some color I love
    SDL_SetRenderDrawColor(renderer, 87, 124, 138, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(renderer);

    // first, get the window size since the window may be resized
    int window_height;
    int window_width;
    SDL_GetRendererOutputSize(renderer, &window_width, &window_height);

    // next, calculate some size of each drawing
    //  on each direction, space is first occupied by two OuterPaddings
    auto window_height_available = static_cast<float>(window_height) - OuterPadding * 2;
    auto window_width_available = static_cast<float>(window_width) - OuterPadding * 2;
    // the sidebar will also take some space away
    window_width_available -= SidebarWidth;
    //  to simplify expression, cast height and width to float first
    auto f_width = static_cast<float>(width);
    auto f_height = static_cast<float>(height);
    //  calculate total space occupied by border and inner padding
    //   there are exactly two InnerPaddings around each cell on both direction and the number of borders is
    //   exactly more than number of cells by 1
    auto total_horizontal_border = 2 * InnerPadding * f_width + (f_width + 1) * BorderWidth;
    auto total_vertical_border = 2 * InnerPadding * f_height + (f_height + 1) * BorderWidth;
    //  the free space we still have can be used for cells
    float cell_width_available = window_width_available - total_horizontal_border;
    float cell_height_available = window_height_available - total_vertical_border;
    //  we want the cells be squares and should fit in the free space, therefore we need to decide their
    //  size
    //   simply divide free space on each direction uniformly to cells and verify if it will can fit the
    //   free space on the other direction, then choose the smaller size
    const float horizontal_proposal = cell_width_available / f_width;
    const float vertical_proposal = cell_height_available / f_height;
    const float final_proposal =
        vertical_proposal * f_width > cell_width_available ? horizontal_proposal : vertical_proposal;
    //  calculate space occupied on each direction by all elements that shall be drawn: borders, inner
    //  paddings and cells
    const float real_total_width = final_proposal * f_width + total_horizontal_border;
    const float real_total_height = final_proposal * f_height + total_vertical_border;

    // then, draw the board
    //  all elements are now drawn as rectangles
    SDL_FRect rect = {
        .x = OuterPadding,
        .y = OuterPadding, // the first border shall start with this coordinate
        .w = real_total_width,
        .h = BorderWidth // we will draw horizontal borders first
    };
    //  draw borders with black
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    for (size_t i = 0; i <= height; i++) { // horizontal borders
      SDL_RenderDrawRectF(renderer, &rect);
      // step coordinate for next border
      rect.y += InnerPadding * 2 + BorderWidth + final_proposal;
    }
    rect.x = OuterPadding;
    rect.y = OuterPadding;
    rect.w = BorderWidth;
    rect.h = real_total_height;
    for (size_t i = 0; i <= width; i++) { // vertical borders
      SDL_RenderDrawRectF(renderer, &rect);
      rect.x += InnerPadding * 2 + BorderWidth + final_proposal;
    }
    //  draw cells
    rect.x = OuterPadding + BorderWidth + InnerPadding;
    rect.y = OuterPadding + BorderWidth + InnerPadding;
    rect.w = final_proposal;
    rect.h = final_proposal;
    // for each row
    for (size_t i = 0, offset = 0; i < width; i++) {
      // reset x coordinate to the begin of line
      rect.x = OuterPadding + BorderWidth + InnerPadding;
      // for each column (for each cell)
      for (size_t j = 0; j < height; j++, offset++) {
        // use black for dead, white for alive
        uint8_t color = grid[offset] == 0 ? 0 : 255;
        SDL_SetRenderDrawColor(renderer, color, color, color, SDL_ALPHA_OPAQUE);
        SDL_RenderFillRectF(renderer, &rect);
        // step x coordinate
        rect.x += final_proposal + BorderWidth + 2 * InnerPadding;
      }
      // step y coordinate
      rect.y += final_proposal + BorderWidth + 2 * InnerPadding;
    }
    // calculate FPS
    // add this frame to the frame queue
    this->time_of_frames.emplace_back(std::chrono::system_clock::now());
    // remove any frame that is generated more than 1 second ago
    using namespace std::chrono_literals;
    while (this->time_of_frames.back() - this->time_of_frames.front() > 1s) {
      this->time_of_frames.pop_front();
    }
    // number of elements in the queue is the number of frames generated in the last second
    const auto fps_information = std::format("{}", this->time_of_frames.size());
    // make a surface to show this counter
    auto surface =
        TTF_RenderUTF8_Solid(this->font, fps_information.c_str(), {255, 255, 255, SDL_ALPHA_OPAQUE});
    const int counter_height = surface->h;
    const int counter_width = surface->w;
    // make it a texture
    auto texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
    // render it at bottom-right
    // the (always changing) counter first, remember to reserve space for itself and the text of unit
    //  we also add a space with 1/4 width of Chinese character between the counter and the unit here
    rect.x = static_cast<float>(window_width) - OuterPadding - static_cast<float>(counter_width) -
             static_cast<float>(this->fps->width) / 3.0f * 3.25f;
    rect.y = static_cast<float>(window_height) - OuterPadding - static_cast<float>(counter_height);
    rect.h = static_cast<float>(counter_height);
    rect.w = static_cast<float>(counter_width);
    SDL_RenderCopyF(renderer, texture, nullptr, &rect);
    // destroy the texture since it may change frequently therefore is hard to reuse
    SDL_DestroyTexture(texture);
    // render the unit in fixed position
    rect.x = static_cast<float>(window_width) - OuterPadding - static_cast<float>(this->fps->width);
    rect.w = static_cast<float>(this->fps->width);
    SDL_RenderCopyF(renderer, this->fps->texture, nullptr, &rect);
    // show what we have drawn on the window with the renderer
    //  the result of render will not show up without this call
    SDL_RenderPresent(renderer);
  }
  [[nodiscard]] bool check_exit() const override {
    SDL_Event event;
    while (SDL_PollEvent(&event) == 1) {
      if (event.type == SDL_KEYDOWN) {
        if (event.key.keysym.sym == SDLK_q) {
          return true; // stop running is Q is pressed
        }
      }
    }
    return false;
  }
};
class TUIFrontEnd final : public FrontEnd {
private:
  static inline void clean_screen() { std::cout << "\033c"; }
  TUIFrontEnd() {
    // initialize TUI
    //  clean up the terminal
    TUIFrontEnd::clean_screen();
  }

public:
  // get the singleton instance
  static TUIFrontEnd &getInstance() {
    static TUIFrontEnd tui_frontend;
    return tui_frontend;
  }
  // nothing todo
  ~TUIFrontEnd() override {
    //  clean up the terminal
    TUIFrontEnd::clean_screen();
  }
  // draw current state of board
  void draw(const Board &board) override {
    // get board information
    auto [width, height, grid] = board.get_grid();

    // clean screen
    TUIFrontEnd::clean_screen();

    // for each row
    for (size_t i = 0, offset = 0; i < width; i++) {
      // for each column (for each cell)
      for (size_t j = 0; j < height; j++, offset++) {
        // use '.' for dead, '*' for alive
        std::cout << (grid[offset] == 0 ? '.' : '+');
      }
      // step y coordinate
      std::cout << '\n';
    }
  }
  [[nodiscard]] virtual bool check_exit() const override {
    // Control-C to exit...
    return false;
  }
};
// select frontend with respect to build options
#if defined(pGoL_USE_SDL)
using RealFrontEnd = SDLFrontEnd;
#elif defined(pGoL_USE_TUI)
using RealFrontEnd = TUIFrontEnd;
#else
#error "exactly one frontend must be specified!"
#endif
int main() {
  // initialize a frontend
  FrontEnd &frontend = RealFrontEnd::getInstance();
  // initialize the board
  Board board(BoardWidth, BoardHeight);
  while (true) {
    // let frontend take care of if we shall exit
    if (frontend.check_exit()) {
      break;
    }
    // let frontend take care of drawing current frame
    frontend.draw(board);
    // update border to next state
    board.update();
    // sleep for a while, a naive way to control FPS
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(200ms);
  }
  return 0;
}